# Org HTML themes with the corporate identity of Inria

[![pipeline status](https://gitlab.inria.fr/mfelsoci/inria-org-html-themes/badges/master/pipeline.svg)](https://gitlab.inria.fr/mfelsoci/inria-org-html-themes/-/commits/master)

We created this repository in the aim of easing the export of Org documents
into HTML using the corporate identity of Inria.

Address your inquiries to [Marek Felsoci](marek.felsoci@inria.fr).

So as the [base project](https://github.com/fniessen/org-html-themes) of
Fabrice Niessen, the sources of the hereby themes are available under the terms
of the [GNU General Public License version 3.0](LICENSE). The Inria logo is a
property of [Inria](https://www.inria.fr/en).

## readtheorginria

The only theme available for now is a clone of the Fabrice Niessen's
[readtheorg](https://github.com/fniessen/org-html-themes#about-readtheorg)
theme that we refer to here as **readtheorginria**.

### Example

[Click here](https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes) to see
a live demo.

### Usage

For beginners, we recommend to go with the online theme setup. Although, if you
do not want your project to depend on an internet connection, you may want to
use the theme offline (see below).

#### Online

This is the most straightforward way of applying an HTML theme to an Org
document. Simply add the following line into the header of the target Org
document.

```
#+SETUPFILE: https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/online-theme-readtheorginria.setup
```

If you want to respect the 80-columns line width, we provide a shortened link
for to access the theme as well:

```
#+SETUPFILE: https://frama.link/readtheorginria
```

#### Offline

It is possible to provide an offline path to an Org HTML theme too. Although,
you must ensure that the sources of the theme (`lib` and `readtheorginria`
folders) are present in the same folder as the Org document you want to apply
the theme to. The `theme-readtheorginria.setup` may be placed anywhere you
want.

Finally, to refer to the theme setup file in the header of your Org document,
you can use a local link as follows.

```
#+SETUPFILE: ./local/path/to/theme-readtheorginria.setup
```
